import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../../shared/employee.service';
import { DepartmentService } from 'src/app/shared/department.service';
import { ProgrammingLanguagesService } from 'src/app/shared/programming-languages.service';
import { NotificationService } from 'src/app/shared/notification.service';
import {MatDialogRef} from '@angular/material/dialog';



@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(public service: EmployeeService, public departmentService: DepartmentService, public ProgrammingService: ProgrammingLanguagesService, public snackBar: NotificationService,
     public dialogRef: MatDialogRef<EmployeeComponent>) { }
  /*ProgrammingLanguages = [
    {id: 1, value: 'Java'},
    {id: 2, value: 'python'},
    {id: 3, value: 'C++'},
    {id:4, value: '.net'}
  ];*/

  ngOnInit() {
    this.service.getEmployees();
  }

  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();

  }
  onSubmit() {
    if(this.service.form.valid) {
      if(!this.service.form.get('$key').value)
      this.service.insertEmployee(this.service.form.value);
      else
      this.service.updateEmployee(this.service.form.value);
      this.service.form.reset();
      this.service.initializeFormGroup();
    this.snackBar.success('::Submitted Successfully');
    this.onClose();

    }
  }
  onClose() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    this.dialogRef.close();
  }
}
