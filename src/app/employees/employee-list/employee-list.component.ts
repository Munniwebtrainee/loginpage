import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/shared/employee.service';
import { DepartmentService } from 'src/app/shared/department.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { EmployeeComponent } from '../employee/employee.component';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  listData: MatTableDataSource<any>;

  constructor( public service: EmployeeService, 
    public departmentService: DepartmentService,
     private dialog: MatDialog ,
     private snackBar: NotificationService
   ) { }

  displayedColumns : string[] = ['Id','Name', 'email', 'mobile', 'city', 'gender', 'department', 'DOJ', 'ProgrammingLanguages', 'actions' ];

  ngOnInit(): void {
    this.service.getEmployees().subscribe(
      list => {
        let array = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
        this.listData = new MatTableDataSource(array);

      });
  }
  onAdd() {
    this.service.initializeFormGroup();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "50%";
    this.dialog.open(EmployeeComponent, dialogConfig)
  }
  onEdit(row) {
    this.service.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "50%";
    this.dialog.open(EmployeeComponent, dialogConfig)
  }
  onDelete($key) {
    if(confirm('are you sure to delete record')){
    this.service.deleteEmployee($key);
    this.snackBar.warn('!deleted successfully');
    }
  }

}
