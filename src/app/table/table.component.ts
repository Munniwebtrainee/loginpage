import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  /*row = [
    {name: 'anusha', email: 'anu@gamil.com', city:'guntur'},
    {name: 'vishnu', email: 'vishnu@gamil.com', city:'tenali'},
    {name: 'anjali', email: 'anju@gamil.com', city:'vizag'},
    {name: 'saileela', email: 'saileela@gamil.com', city:'tuni'}
  
    ];*/
TableData : any = [];
    personList :Array<any> =  [
      {id: 1, name: 'anusha', email: 'anu@gamil.com', city:'guntur'},
      {id: 2,name: 'vishnu', email: 'vishnu@gamil.com', city:'tenali'},
      {id: 3, name: 'anjali', email: 'anju@gamil.com', city:'vizag'},
      {id: 4, name: 'saileela', email: 'saileela@gamil.com', city:'tuni'}
    
      ];
      awaitingPersonList: Array<any> = [
        {id: 5,name: 'munni', email: 'munni@gmail.com', city:'vzm'},
        {id: 6,name: 'anitha', email: 'anithai@gmail.com', city:'vzm'},
        {id: 7,name: 'vasu', email: 'vasu@gmail.com', city:'vzm'},
        {id: 8,name: 'avinash', email: 'avinash@gmail.com', city:'vzm'},
      ]
      
      addRow() {
        if (this.awaitingPersonList.length > 0) {
          const person = this.awaitingPersonList[0];
          this.personList.push(person);
          this.awaitingPersonList.splice(0, 1);
        }
      }
    
  deleteRow(id: any){
    this.awaitingPersonList.push(this.personList[id]);
  this.personList.splice(id, 1); 
   /* var delBtn = confirm(" Do you want to delete ?");
    if ( delBtn == true ) {
      this.row.splice(x, 1 );
    }   */
  }

  constructor() { }

  ngOnInit(): void {
  }

}
