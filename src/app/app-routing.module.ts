import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginpageComponent} from './loginpage/loginpage.component';
import { HomeComponent } from './home/home.component';
import {AuthGuard} from './auth.guard';
import {AboutComponent} from './about/about.component';
import { LagoutComponent } from './lagout/lagout.component';


const routes: Routes = [
  {path: '', redirectTo:'/home', pathMatch: 'full' },
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'about', component: AboutComponent},
  {path: 'loginpage', component : LoginpageComponent},
  {path:'logout', component: LagoutComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
