import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ProgrammingLanguagesService {
  ProgrammingLanguagesList: AngularFireList<any>;
  array = [];

  constructor(private firebase: AngularFireDatabase) {
    this.ProgrammingLanguagesList = this.firebase.list('programming');
    this.ProgrammingLanguagesList.snapshotChanges().subscribe(
      list => {
        this.array = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          }
          })
        })
   }
}
