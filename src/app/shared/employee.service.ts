import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import * as _ from 'lodash';
import { DatePipe } from '@angular/common';

export class Form {
  Id: number;
  Name: string;
  email: string;
  mobile: number;
  city: string;
  gender: number;
  department: string;
  Dateofjoin: Date;
  programmingLanguages: string;
}

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
/*private url : string = "/assets/user.json";
  constructor(private http: HttpClient) { 
    this.http.get(this.url).toPromise().then(data => {
      console.log(Form);
    });
  }*/

  constructor( private firebase: AngularFireDatabase, private datePipe: DatePipe) { }
  employeeList : AngularFireList<any>;

  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    Id: new FormControl('', Validators.required),
    Name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.email , Validators.required]),
    mobile: new FormControl('', [Validators.required, Validators.minLength(8)]),
    city: new FormControl(''),
    gender: new FormControl('1'),
    department: new FormControl(0),
    DOJ: new FormControl(''),
    ProgrammingLanguages: new FormControl(0)
  });

  initializeFormGroup() {
    this.form.setValue({
      $key: null,
      Id: '',
      Name: '',
      email: '',
      mobile: '',
      city: '',
      gender: '1',
      department: 0,
      DOJ: '',
      ProgrammingLanguages:0
    });
  }
  getEmployees () {
    this.employeeList = this.firebase.list('employees');
    return this.employeeList.snapshotChanges();
  }
  insertEmployee(employee) {
    this.employeeList.push({
      Id: employee.Id,
      Name: employee.Name,
      email: employee.email,
      mobile: employee.mobile,
      city: employee.city,
      gender: employee.gender,
      department: employee.department,
      DOJ: employee.DOJ == "" ? "" : this.datePipe.transform(employee.DOJ, 'yyyy-MM-dd'),
    ProgrammingLanguages: employee.ProgrammingLanguages
    });
  }
  updateEmployee(employee) {
this.employeeList.update(employee.$key, {
  Id: employee.Id,
  Name: employee.Name,
  email: employee.email,
  mobile: employee.mobile,
  city: employee.city,
  gender: employee.gender,
  department: employee.department,
  DOJ: employee.DOJ == "" ? "" : this.datePipe.transform(employee.DOJ, 'yyyy-MM-dd'),
  ProgrammingLanguages: employee.ProgrammingLanguages
});
  }
  deleteEmployee($key: string) {
    this.employeeList.remove($key);
  }
  populateForm(employee) {
    this.form.setValue(_.omit(employee, ''));
  }
  
}