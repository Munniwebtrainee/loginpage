import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {LoginService} from '../login.service';

export class Login {
  username: string;
    email: string;
    password: string;
}
@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
  user: Login = new Login();
  loginForm : FormGroup;
  hide = true;
  isSubmitted = false;

  constructor(private formBuilder: FormBuilder ,  private route: ActivatedRoute,
    private router: Router, private login: LoginService) { }
    
      FormGroup  = new FormGroup({
  username : new FormControl('', [Validators.required]),
  email : new FormControl('', [Validators.required, Validators.email]),
  password : new FormControl ('', [Validators.required])
      });

  ngOnInit() {
    this.loginForm  = this.formBuilder.group({
      'username': [this.user.username, [
        Validators.required
      ]],
      'email': [this.user.email, [
        Validators.required,
        Validators.email
      ]],
      'password': [this.user.password, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20)
      ]]
    });
    
    
  }
  
  onLoginSubmit() {
    if(this.loginForm.valid){
    this.isSubmitted = true;
    if(this.login.onLogin()){
      this.router.navigate(['/home']);
    }
  }
    else {
    window.alert('enter values')

    }
  }
  
}


