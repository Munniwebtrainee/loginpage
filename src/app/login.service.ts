import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
@Injectable({
  providedIn: 'root'
})
export class LoginService {
isSubmitted = false;

  constructor() { }
  form: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    Name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.email , Validators.required]),
  });
  onLogin() {
    this.isSubmitted = true;
    return true;
  }

}
