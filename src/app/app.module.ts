import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {environment} from '../environments/environment';
import {DatePipe} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import { HomeComponent } from './home/home.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import { TableComponent } from './table/table.component';
import {MatTableModule} from '@angular/material/table';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeComponent } from './employees/employee/employee.component';
import {EmployeeListComponent} from './employees/employee-list/employee-list.component';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {EmployeeService} from './shared/employee.service';
import {LoginService} from './login.service';
import { AboutComponent } from './about/about.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import { LagoutComponent } from './lagout/lagout.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {DepartmentService} from './shared/department.service';
import {ProgrammingLanguagesService} from './shared/programming-languages.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NotificationService} from './shared/notification.service';





@NgModule({
  declarations: [
    AppComponent,
    LoginpageComponent,
    HomeComponent,
    TableComponent,
    EmployeesComponent,
    EmployeeComponent,
    AboutComponent,
    MainNavComponent,
    LagoutComponent,
    EmployeeListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule, 
    BrowserAnimationsModule,
    MatToolbarModule,
    HttpClientModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatTableModule,
    MatGridListModule,
    MatSelectModule,
    MatCheckboxModule,
    MatNativeDateModule, 
    MatRippleModule,
    MatRadioModule,
    MatDatepickerModule,
    LayoutModule,
    MatSidenavModule,
    MatListModule,
    MatMenuModule,
    AngularFireDatabaseModule,
    MatSnackBarModule,
    MatTableModule,
    AngularFireModule.initializeApp(environment.firebaseConfig)
    

  ],
  providers: [EmployeeService,LoginService, DepartmentService, ProgrammingLanguagesService, NotificationService, DatePipe ],
  bootstrap: [AppComponent],
  entryComponents: [EmployeeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule { }
