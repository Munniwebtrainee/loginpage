import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LagoutComponent } from './lagout.component';

describe('LagoutComponent', () => {
  let component: LagoutComponent;
  let fixture: ComponentFixture<LagoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LagoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LagoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
