import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {LoginService} from '../login.service';

export class Login {
  username: string;
    email: string;
    password: string;
}
@Component({
  selector: 'app-lagout',
  templateUrl: './lagout.component.html',
  styleUrls: ['./lagout.component.css']
})
export class LagoutComponent implements OnInit {
  user: Login = new Login();
  opened = false;
  hide = true;
  isSubmitted = false;
  loginForm : FormGroup;

  constructor(private formBuilder: FormBuilder ,  private route: ActivatedRoute,
    private router: Router, private login: LoginService) { }

    FormGroup  = new FormGroup({
      username : new FormControl('', [Validators.required]),
      email : new FormControl('', [Validators.required, Validators.email]),
      password : new FormControl ('', [Validators.required])
          });

  ngOnInit(): void {
    this.loginForm  = this.formBuilder.group({
      'username': [this.user.username, [
        Validators.required
      ]],
      'email': [this.user.email, [
        Validators.required,
        Validators.email
      ]],
      'password': [this.user.password, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20)
      ]]
    });
  }
  onLoginSubmit() {
    if(this.loginForm.valid){
    this.isSubmitted = true;
    if(this.login.onLogin()){
      this.router.navigate(['/home']);
    }
  }
    else {
    window.alert('enter values')

    }
  }

}
