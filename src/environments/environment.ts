// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB5uVFdvqg1TDYlGtCbzC-k6kYHHM92FGw",
  authDomain: "amcrud-5fffb.firebaseapp.com",
  databaseURL: "https://amcrud-5fffb.firebaseio.com",
  projectId: "amcrud-5fffb",
  storageBucket: "amcrud-5fffb.appspot.com",
  messagingSenderId: "1098716554542",
  appId: "1:1098716554542:web:a1dc8bacaf8e56b06446bf",
  measurementId: "G-BN9E9R4QVK"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
